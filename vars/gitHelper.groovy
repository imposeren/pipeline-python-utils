def getBranch() {
	sh(returnStdout: true, script: "cd ${env.WORKSPACE}; git name-rev HEAD | cut -f2 -d ' ' | cut -d'/' -f 3").trim()
}