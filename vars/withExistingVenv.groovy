
def call(Closure body) {
	verifyVenvRequirements()
    withEnv(["VIRTENV_DIR=${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.BUILD_NUMBER}",
             "PATH+VENV=${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.BUILD_NUMBER}/bin:~/.local/bin"]) {
        body()
        sh '/usr/bin/virtualenv --python=python${PYTHON_VERSION} "${VIRTENV_DIR}"'
        //sh '~/.local/bin/virtualenv-tools --update-path "${VIRTENV_DIR} "${VIRTENV_DIR}"'
    }
}
