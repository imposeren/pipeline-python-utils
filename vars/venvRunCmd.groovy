def call(String pycmd) {
	verifyVenvRequirements("-f \"\${VIRTENV_DIR}/bin/python${env.PYTHON_VERSION}\"", 'Virtualenv directory does not exist')
    sh "~/.local/bin/vex --path=\"\${VIRTENV_DIR}\" ${pycmd}"
}
