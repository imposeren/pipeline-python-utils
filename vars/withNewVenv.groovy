
def call(Closure body) {
	echo "Initializing virtualenv"
	verifyVenvRequirements()
	sh "${env.PYTHON_PIP_CMD} install --user vex"

    withEnv(["VIRTENV_DIR=${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.BUILD_NUMBER}",
             "PATH+VENV=${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.BUILD_NUMBER}/bin:~/.local/bin"]) {
	    sh '/usr/bin/virtualenv --download --always-copy --python=python${PYTHON_VERSION} "${VIRTENV_DIR}"'
	    sh '/usr/bin/virtualenv --python=python${PYTHON_VERSION} "${VIRTENV_DIR}"'
	    echo "Initialized virtualenv"
        body()
        sh '/usr/bin/virtualenv --python=python${PYTHON_VERSION} "${VIRTENV_DIR}"'
        // sh '~/.local/bin/virtualenv-tools --update-path "${VIRTENV_DIR}" "${VIRTENV_DIR}"'
    }
}
