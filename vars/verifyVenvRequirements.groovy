def call(String condition='-n "${VIRTENV_NAME}"', String message='Environment variable VIRTENV_NAME should be non-zero') {
    sh "test '${condition}' || (echo '${message}'; exit 1)"
}